-- Conway's game of life in Elm by Robert Woodhead (trebor@animeigo.com)
--
-- I used this as a learning tool to get more comfortable with Elm. As such, it
-- is pedantically over-commented, but that may be helpful to other newbies.


port module Main exposing (..)
import Browser
import Html exposing (Html, div, text, br, button, input, fieldset, p, i, span, output, textarea, ul, li, a, table, tbody, tr, td)
import Html.Attributes as Attr exposing (class, style, type_, checked, cols, rows, name, title, attribute, href, target)
import Html.Events as Events exposing (onInput, onClick)
import Html.Events.Extra.Mouse as Mouse
import Svg exposing (Svg, rect)
import Svg.Attributes as SAttr exposing (x, y, fill, viewBox, id, cursor)
import Time
import Json.Decode
import VirtualDom
import List.Extra exposing (transpose)
import Char
import Random
import Random.List

import Parser exposing (..)


--import Debug exposing (log)
-----------------------------------------------------
--
-- Types and Constants
--
----------------------------------------------------


type Cell
    = Alive
    | Dead


type alias Board =
    List (List Cell)


type alias BoardCount =
    List (List Int)



-- Size of board in internal coordinate system (scaled when SVG is displayed)
-- Currently, boards always have equal width and height.


boardDim : Int
boardDim =
    4096



-- Minimum cell size in internal coordinate system.


minCellSize : Int
minCellSize =
    4



-- Size range of a board in cells (both horizontal and vertical)
-- These sizes should be divisible by 2 for smooth zooming.


minBoardSize : Int
minBoardSize =
    64


maxBoardSize : Int
maxBoardSize =
    boardDim // minCellSize



-- Full Speed slider value (for running with no delay)


fullSpeed : Int
fullSpeed =
    20



-----------------------------------------------------
--
-- Model and Msgs
--
----------------------------------------------------


type Mode
    = Playing
    | Import
    | Export
    | About
    | Patterns


type InputMode
    = UsingMouse
    | UsingTouch


type Direction
    = Up
    | Down
    | Left
    | Right
    | Clockwise

type Msg
    = Tick Time.Posix
    | BoardLocation ObjectLocation
    | Speed String
    | BoardSize String
    | SetMode Mode
    | SetInputMode InputMode
    | ImportBoard String
    | ProcessImport
    | SetPattern Int
    | ClearBoard
    | MakeSoup

type alias Model =
    { board : Board
    , headers : List String
    , paintRow : Int
    , paintCol : Int
    , paintColor : Cell
    , painting : Bool
    , boardLocation : ObjectLocation
    , wrap : Bool
    , running : Bool
    , speed : Int
    , mode : Mode
    , inputMode : InputMode
    , importedBoard : String
    , seed : Random.Seed
    , ratio: Float }

type alias Point
    = {x: Int, y: Int}

type alias MaybePoint
    = {x: Maybe Int, y: Maybe Int}

type alias Position 
    = (Float, Float)

type alias PatternMetadata =
    { name: String
    , author: String
    , description: String
    , url: String }

-----------------------------------------------------
--
-- App Initialization
--
----------------------------------------------------
-- We get some initialization information passed to us when we start up,
-- so we can set the inputMode correctly.

type alias Flags =
    { hasTouch : Bool
    , randomSeed : Float
    , ratio : Float }


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        -- Use the first builtin board
        -- pattern =
        --     List.head builtinBoards

        generator = Random.List.choose builtinBoards
        seed = Random.initialSeed (round flags.randomSeed)
        ((pattern, _), _) = Random.step generator seed 

        ( headers, board ) =
            loadBoard
                (case pattern of
                    Just str ->
                        str
                    _ ->
                        ""
                )

        cellsize =
            max minCellSize (boardDim // List.length board)

        -- Set the appropriate input mode for the device
        inputMode =
            if flags.hasTouch then
                UsingTouch
            else
                UsingMouse
    in
        ( { board = board
          , headers = headers
          , paintRow = 0
          , paintCol = 0
          , paintColor = Alive
          , painting = True
          , boardLocation = ObjectLocation 0 0 0 0
          , wrap = False
          , running = True
          , speed = 12
          , mode = Playing
          , inputMode = inputMode
          , importedBoard = ""
          , seed = Random.initialSeed (round flags.randomSeed)
          , ratio = flags.ratio
          }
        , initialLocation ()
        )



-- Always listen for incoming messages about board dimension changes,
-- and optionally get ticks if we are free-running the board.
-- In addition, if we are using a mouse, we need to subscribe to
-- some mouse events


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ boardLocation BoardLocation
        , if model.running && model.speed > 0 then
            Time.every 1000 Tick
          else
            Sub.none
        
        -- , if model.inputMode == UsingMouse && model.mode == Playing then
        --     if model.painting then
        --         Sub.batch
        --             [ Mouse.onUp (\event -> MouseUp event.clientPos)
        --             , Mouse.onMove (\event -> MouseMove event.clientPos)
        --             ]
        --     else
        --         Mouse.onDown (\event -> MouseDown event.clientPos)
        --   else
        --     Sub.none
        ]



-- Data structure returned by the boardLocation port.


type alias ObjectLocation =
    { top : Int, left : Int, width : Int, height : Int }



-- Outbound port for querying the board dimensions.


port initialLocation : () -> Cmd msg



-- Inbound port for receving updates on the dimensions.


port boardLocation : (ObjectLocation -> msg) -> Sub msg



-----------------------------------------------------
--
-- Update
--
----------------------------------------------------


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Move to next generation (explicit command or subscription tick)
        Tick time ->
            ( generation model, Cmd.none )

        -- Move between the playing, export and import modes
        SetMode mode ->
            ( { model
                | mode = mode
                , running = False
                , painting = False
              }
            , Cmd.none
            )

        -- Change the input mode (tablet or mouse)
        SetInputMode inputMode ->
            ( { model
                | inputMode = inputMode
                , running = False
                , painting = False
              }
            , Cmd.none
            )

        -- Clear the board but maintain size
        ClearBoard ->
            let
                board =
                    List.map (List.map (\c -> Dead)) model.board

                result =
                    { model | board = board, painting = False }
            in
                ( result, Cmd.none )

        MakeSoup ->
            let
                ( seed, board ) =
                    makeSoup model.seed model.board

                result =
                    { model
                        | board = board
                        , seed = seed
                        , painting = False
                    }
            in
                ( result, Cmd.none )

        -- Speed of free run
        Speed v ->
            let
                speed =
                    String.toInt v
                        |> Maybe.withDefault 0

                result =
                    { model
                        | speed = speed
                        , painting = False
                    }
            in
                ( result, Cmd.none )

        -- Zoom the board
        BoardSize v ->
            let
                -- We always change size by 2 cells so the zooming is even
                halfsize =
                    String.toInt v
                        |> Maybe.withDefault (maxBoardSize // 2)

                boardsize =
                    max minBoardSize (2 * halfsize)

                curdim =
                    List.length model.board

                board =
                    resizeBoard boardsize model.board

                result =
                    { model
                        | board = board
                        , painting = False
                    }
            in
                ( result, Cmd.none )


        -- Update the location of the board (incoming message from port)
        BoardLocation location ->
            let
                result =
                    { model
                        | boardLocation = location
                        , painting = False
                    }
            in
                ( result, Cmd.none )

        -- As changes are made in the import textarea, we get ImportBoard
        -- messages which we use to update the importedBoard string.
        ImportBoard pattern ->
            let
                result =
                    { model | importedBoard = pattern }
            in
                ( result, Cmd.none )

        -- Then when the user clicks the Import button, this message
        -- fires and we actually process the board.
        ProcessImport ->
            let
                ( headers, board ) =
                    loadBoard model.importedBoard

                result =
                    { model
                        | board = board
                        , headers = headers
                        , mode = Playing
                        , importedBoard = ""
                        , painting = False
                    }
            in
                ( result, Cmd.none )

        -- Almost identical to ImportBoard, but we grab a built-in board.
        SetPattern index ->
            let
                pattern =
                    builtinBoards
                        |> List.drop index
                        |> List.head
                        |> Maybe.withDefault ""

                ( headers, board ) =
                    loadBoard pattern

                result =
                    { model
                        | board = board
                        , headers = headers
                        , mode = Playing
                        , importedBoard = ""
                        , painting = False
                    }
            in
                ( result, Cmd.none )



-- Transform from mouse position coordinates to Board row,col coordinates, and
-- check if they are valid. Return a (row,col,valid) tuple.


transformCoordinates : Model -> Position -> ( Int, Int, Bool )
transformCoordinates model position =
    let
        (x, y) = position
        -- x and y pixel positions relative to the board location
        xrp = x - (toFloat model.boardLocation.left)
        yrp = y - (toFloat model.boardLocation.top)

        -- x and y positions scaled to 0 <= x,y < 1 range (if valid!)
        xscale = xrp / (toFloat model.boardLocation.width)
        yscale = yrp / (toFloat model.boardLocation.height)

        valid =
            xscale >= 0 && xscale < 1.0 && yscale >= 0 && yscale < 1.0 && model.mode == Playing

        -- length of the board (both in x and y)
        lmb =
            toFloat (List.length model.board)

        -- row and col of mouse position in board coordinates
        col =
            floor (xscale * lmb)

        row =
            floor (yscale * lmb)
    in
        ( row, col, valid )



-----------------------------------------------------
--
-- Life Functions
--
----------------------------------------------------
--
-- Step a generation


type alias VerticalCountShifter =
    Int -> List (List Int) -> List (List Int)


type alias HorizontalCountShifter =
    Int -> List Int -> List Int


generation : Model -> Model
generation model =
    let
        mbs =
            List.length model.board

        -- If we are not wrapping the board, then we may need to expand it.
        --
        -- Note: one nice thing about let blocks is that you can define local
        -- functions inside of them. I have a personal convention of always
        -- ending them with the _ character as a cue about where to look.
        -- startBoard =
        --     if (not model.wrap) && (mbs < maxBoardSize) && hasEdgeCell_ model.board then
        --         resizeBoard (mbs + 2) model.board
        --     else
        --         model.board
        startBoard = model.board

        -- Curry the vertical and horizontal count shifter functions. This
        -- lets us use a single function to count all the neighboring cells
        ( vshift, hshift ) =
            if model.wrap then
                ( rotateList
                , rotateList
                )
            else
                ( shiftList (List.repeat (List.length startBoard) 0)
                , shiftList 0
                )

        -- Count the neighboring cells, passing in the shifting functions
        neighbors =
            countCells vshift hshift startBoard

        -- Evolve the next generation
        newBoard =
            evolveBoard startBoard neighbors

        result =
            { model | board = newBoard }

        -- Does the board have a live cell on its perimeter?
        hasEdgeCell_ : Board -> Bool
        hasEdgeCell_ board =
            let
                bsm1 =
                    mbs - 1

                top =
                    case List.head board of
                        Just row ->
                            List.member Alive row

                        Nothing ->
                            False

                bottom =
                    case List.head (List.drop bsm1 board) of
                        Just row ->
                            List.member Alive row

                        Nothing ->
                            False

                sides =
                    List.foldl rowEdgeCell_ False board

                -- Does a row have a live cell on its edges? Note that this is
                -- a local function of a local function!
                rowEdgeCell_ : List Cell -> Bool -> Bool
                rowEdgeCell_ row carry =
                    let
                        left =
                            case List.head row of
                                Just cell ->
                                    cell == Alive

                                Nothing ->
                                    False

                        right =
                            case List.head (List.drop bsm1 row) of
                                Just cell ->
                                    cell == Alive

                                Nothing ->
                                    False
                    in
                        carry || left || right
            in
                top || bottom || sides
    in
        result



-- Set each element in the board to the count of live cells
-- in itself and its eight neighbors, with optional wrapping
-- at the edges. Note that this changes the survival
-- condition because the count includes the potentially
-- surviving cell.


countCells : VerticalCountShifter -> HorizontalCountShifter -> Board -> BoardCount
countCells vshift hshift board =
    let
        -- Convert Cells to integer counts
        boardcount =
            List.map toRowCount_ board

        -- Create left and right shifted or rotated boards
        rl =
            List.map (hshift 1) boardcount

        rr =
            List.map (hshift -1) boardcount

        -- Sum the board with the shifted versions
        rowNeighbors =
            List.map3 rowSum_ boardcount rl rr

        -- Create up and down shifted counts
        ru =
            vshift 1 rowNeighbors

        rd =
            vshift -1 rowNeighbors

        -- Sum everything (the final result)
        result =
            List.map3 rowSum_ rowNeighbors rd ru

        -- Cell to Integer conversion
        toRowCount_ : List Cell -> List Int
        toRowCount_ row =
            List.map
                (\cell ->
                    if cell == Alive then
                        1
                    else
                        0
                )
                row

        -- Add up 3 lists
        rowSum_ : List Int -> List Int -> List Int -> List Int
        rowSum_ row adj1 adj2 =
            List.map3 cellSum_ row adj1 adj2

        -- Add 3 numbers
        cellSum_ : Int -> Int -> Int -> Int
        cellSum_ cell adj1 adj2 =
            cell + adj1 + adj2
    in
        result



-- Cell Getter and Setter


getCell : Board -> Int -> Int -> Cell
getCell board row col =
    board
        |> List.drop row
        |> List.head
        |> Maybe.withDefault []
        |> List.drop col
        |> List.head
        |> Maybe.withDefault Dead


setCell : Board -> Int -> Int -> Cell -> Board
setCell board row col newcell =
    let
        setRow_ rownum therow =
            if rownum /= row then
                therow
            else
                List.indexedMap setColumn_ therow

        setColumn_ colnum cell =
            if colnum /= col then
                cell
            else
                newcell
    in
        List.indexedMap setRow_ board



-- These rotate and shift functions have generic type signatures.
-- This is because we also use them when the user shifts/rotates the
-- board, so they need to handle List Cell and List Int.


rotateList : Int -> List a -> List a
rotateList rot list =
    let
        r = modBy rot (List.length list)
    in
        (List.drop r list) ++ (List.take r list)


shiftList : a -> Int -> List a -> List a
shiftList blank rot list =
    if rot >= 0 then
        (List.drop rot list) ++ (List.repeat rot blank)
    else
        (List.repeat -rot blank) ++ (List.take ((List.length list) + rot) list)



-- Evolve a board. A dead cell becomes alive if it has 3 and only 3
-- neighboring live cells; a live cell survives if it has 2 or 3
-- neighboring live cells.


evolveBoard : Board -> BoardCount -> Board
evolveBoard board neighbors =
    let
        -- Neighbors counts include the current cell, so the "survive"
        -- states are not quite what you expect.
        evolveRow_ : List Cell -> List Int -> List Cell
        evolveRow_ row n =
            List.map2 evolveCell_ row n

        evolveCell_ : Cell -> Int -> Cell
        evolveCell_ cell n =
            if n == 3 || ((cell == Alive) && (n == 4)) then
                Alive
            else
                Dead
    in
        List.map2 evolveRow_ board neighbors



-- Make a soup board of 11.111% live cells. This is about the right ratio to
-- maximize generation of interesting patterns according to this note:
-- https://sourceforge.net/p/golly/discussion/467856/thread/2ea0e461/
--
-- To ensure a different pattern each time, we pass in the current model.seed
-- value and return the updated value. This is how Random generators maintain
-- their state between invocations.


makeSoup : Random.Seed -> Board -> ( Random.Seed, Board )
makeSoup seed board =
    let
        -- Random generator that produces an Alive cell 1/9th of the time.
        soupCell =
            Random.map
                (\i ->
                    if i == 0 then
                        Alive
                    else
                        Dead
                )
                (Random.int 0 8)

        -- size of the board
        dim =
            List.length board

        -- Random generator that produces a row of soup cells
        soupRow =
            Random.list dim soupCell

        -- Random generator that produces a board of soup cells
        soupBoard =
            Random.list dim soupRow

        -- Create a soup board and a new seed
        ( newboard, newseed ) =
            Random.step soupBoard seed
    in
        ( newseed, newboard )



-- Load a board. First we try RLE format, and if that doesn't work, we
-- try Cell format.


loadBoard : String -> ( List String, Board )
loadBoard pattern =
    let
        ( h1, c1 ) =
            decodeRLE pattern

        ( headers, cells ) =
            if c1 == [] then
                decodeCELL pattern
            else
                ( h1, c1 )

        w =
            List.head cells
                |> Maybe.withDefault []
                |> List.length

        h =
            List.length cells

        boardsize =
            max minBoardSize (8 + max w h)

        board =
            resizeBoard boardsize cells
    in
        ( headers, board )



-- Decode a board from the LifeWiki .cell format into a list of the headers
-- and a Board. See: http://www.conwaylife.com/wiki/Plaintext


decodeCELL : String -> ( List String, Board )
decodeCELL template =
    let
        -- Kill leading and trailing whitespace, then split into headers and actual cells.
        ( comments, boardcells ) =
            template
                |> String.trim
                |> String.lines
                |> List.partition (String.startsWith "!")

        -- Get rid of the ! characters on the header lines.
        headers =
            List.map (String.dropLeft 1) comments

        -- Get maximum width.
        mw =
            List.map String.length boardcells
                |> List.maximum
                |> Maybe.withDefault 0

        -- Pad short rows to the max width.
        padshort =
            List.map (String.padRight mw '.') boardcells

        -- The basic board, trimmed to minimum size.
        board =
            List.map rowToCell_ padshort

        -- Local helper functions.
        rowToCell_ : String -> List Cell
        rowToCell_ row =
            List.map strToCell_ (String.toList row)

        strToCell_ : Char -> Cell
        strToCell_ char =
            if (char == '.') || (char == ' ') then
                Dead
            else
                Alive
    in
        ( headers, board )


{- 
    Decode a board from the LifeWiki .RLE format into a list of the headers
    and a Board. See: http://www.conwaylife.com/wiki/RLE

    Example board:
        #N Gosper glider gun
        #C This was the first gun discovered.
        #C As its name suggests, it was discovered by Bill Gosper.
        x = 36, y = 9, rule = B3/S23
        24bo$22bobo$12b2o6b2o12b2o$11bo3bo4b2o12b2o$2o8bo5bo3b2o$2o8bo3bob2o4b
        obo$10bo5bo7bo$11bo3bo$12b2o!
--}

whitespace : Parser ()
whitespace =
    chompWhile (\c -> c == ' ')

xDimensions: Parser (Maybe Int)
xDimensions = 
    oneOf
        [ succeed String.toInt
            |. symbol "x"
            |. whitespace
            |. symbol "="
            |. whitespace
            |= (getChompedString <| chompWhile Char.isDigit)
            |. whitespace
            |. symbol ","
            |. whitespace
        , succeed Nothing
        ]

yDimensions: Parser (Maybe Int)
yDimensions = 
    oneOf
        [ succeed String.toInt
            |. symbol "y"
            |. whitespace
            |. symbol "="
            |. whitespace
            |= (getChompedString <| chompWhile Char.isDigit)
            |. whitespace
            |. symbol ","
            |. whitespace
        , succeed Nothing
        ]

boardSizeParser : Parser (MaybePoint)
boardSizeParser =
    succeed MaybePoint
        |= xDimensions
        |= yDimensions

    -- oneOf
    --     [ succeed Just
    --         |. whitespace
    --         |. symbol "+"
    --         |= int
    --         |. whitespace
    --     , succeed Nothing
    --     ]


decodeRLE : String -> ( List String, Board )
decodeRLE template =
    let
        -- Kill blank lines, leading and trailing whitespace, then split into headers and pattern.
        ( comments, pattern ) =
            template
                |> String.lines
                |> List.filter (\s -> s /= "")
                |> List.map String.trim
                |> List.partition (String.startsWith "#")

        -- Remap RLE headers to .CELL-style headers.
        headers =
            comments
                |> List.map fixRLEHeaders
                |> List.filter (\s -> s /= "")

        -- First line of pattern must be size header. I'm going crazy with pattern-matching case
        -- statements here as a learning exercise.
        board =
            case pattern of
                header :: body ->
                    let
                        -- Extract the size of the pattern from the header.
                        dimensions = Parser.run boardSizeParser header
                            
                                -- |> Regex.split Regex.All (Regex.regex "[\\D]+")
                                -- |> List.drop 1
                                -- |> List.map String.toInt
                                -- |> List.map (Result.withDefault 0)
                                -- |> flip List.append [ 0, 0 ]
                                -- |> List.take 2
                    in
                        []
                            

                _ ->
                    []
    in
        ( headers, board )



-- Convert RLE to CELL format. This is basically a fancy foldl with two accumulators,
-- one for the output string, and one for the current multiplier. If the input RLE
-- is bad the results are undefined.


rleToCell : String -> Int -> List Char -> String
rleToCell acc mult rle =
    case rle of
        cur :: rest ->
            case cur of
                -- Emit mult blank cells (minimum of 1), reset mult to 0, continue.
                'b' ->
                    rleToCell (acc ++ String.repeat (max mult 1) ".") 0 rest

                -- Emit mult live cells (minimum of 1), reset mult to 1, continue.
                'o' ->
                    rleToCell (acc ++ String.repeat (max mult 1) "O") 0 rest

                -- Emit mult endoflines (minimum of 1), reset mult to 0, continue.
                '$' ->
                    rleToCell (acc ++ String.repeat (max mult 1) "\n") 0 rest

                -- If we have a digit, shift it into the mult, continue.
                -- If it isn't a digit, just continue.
                _ ->
                    if Char.isDigit cur then
                        let
                            digit =
                                cur
                                    |> String.fromChar
                                    |> String.toInt
                                    |> Maybe.withDefault 0
                        in
                            rleToCell acc (mult * 10 + digit) rest
                    else
                        rleToCell acc mult rest

        -- If we reach the end, then we are done.
        [] ->
            acc



-- Remap RLE-style headers to CELL-style.


fixRLEHeaders : String -> String
fixRLEHeaders hdr =
    let
        remap_ : String -> String -> String -> String
        remap_ prefix repl line =
            if String.startsWith prefix (String.toUpper line) then
                line
                    |> String.dropLeft (String.length prefix)
                    |> String.trim
                    |> (++) repl
            else
                line

        kill_ : String -> String -> String
        kill_ prefix line =
            if String.startsWith prefix (String.toUpper line) then
                ""
            else
                line
    in
        hdr
            |> remap_ "#C" "!"
            |> remap_ "#N" "!Name: "
            |> remap_ "#O" "!Author: "
            |> kill_ "#"



-- Resize a board to a desired (square) dimension.


resizeBoard : Int -> Board -> Board
resizeBoard tosize board =
    let
        h =
            List.length board

        w =
            case List.head board of
                Just row ->
                    List.length row

                Nothing ->
                    0

        -- Figure out the magnitude of the width change
        wd =
            abs (tosize - w)

        -- Split into left and right halves, but try to balance where the remainder goes,
        -- if we happen to be changing by an odd number of cells (which we shouldn't!)
        ( l, r ) =
            let
                wd2 =
                    wd // 2
            in
                if modBy tosize 2 == 0 then
                    ( wd2, wd - wd2 )
                else
                    ( wd - wd2, wd2 )

        -- Adjust width of rows
        rewidthed =
            if tosize > w then
                List.map padRow_ board
            else if tosize < w then
                List.map trimRow_ board
            else
                board

        -- Now do the same for the height change
        ld =
            abs (tosize - h)

        ( t, b ) =
            let
                ld2 =
                    ld // 2
            in
                if modBy tosize 2 == 0 then
                    ( ld2, ld - ld2 )
                else
                    ( ld - ld2, ld2 )

        deadrow =
            List.repeat tosize Dead

        result =
            if tosize > h then
                List.concat
                    [ List.repeat t deadrow
                    , rewidthed
                    , List.repeat b deadrow
                    ]
            else if tosize < h then
                rewidthed
                    |> List.drop t
                    |> List.take tosize
            else
                rewidthed

        -- Note that our local functions have access to values
        -- defined in the let block, so we don't need to pass them!
        padRow_ : List Cell -> List Cell
        padRow_ row =
            List.concat
                [ List.repeat l Dead
                , row
                , List.repeat r Dead
                ]

        trimRow_ : List Cell -> List Cell
        trimRow_ row =
            row
                |> List.drop l
                |> List.take tosize
    in
        result



-- Trim a board to its minimum bounding rectangle



-- Convert board to .cell text representation.


boardToStr : Board -> String
boardToStr board =
    let
        rowToStr_ : List Cell -> String
        rowToStr_ row =
            List.map
                (\c ->
                    if c == Alive then
                        'O'
                    else
                        '.'
                )
                row
                |> String.fromList
                |> dotTrim_

        dotTrim_ : String -> String
        dotTrim_ s =
            if String.endsWith "." s then
                dotTrim_ (String.dropRight 1 s)
            else
                s
    in
        List.map rowToStr_ board
            |> String.join "\n"



-----------------------------------------------------
--
-- View -- uses Bootstrap.css with some custom css tweaks
-- in app.css for things like styling the sliders and
-- tweaking buttons and toolbars.
--
----------------------------------------------------
--
-- view helpers


slider : (String -> msg) -> String -> Int -> Int -> Int -> Html msg
slider msg cname minv maxv value =
    div [ id "speed-slider" ] [
        (text "speed:")
        ,input
            [ type_ "range"
            , name cname
            , title cname
            , Attr.min (String.fromInt minv)
            , Attr.max (String.fromInt maxv)
            , Attr.value (String.fromInt value)
            , onInput msg
            ] [] ]


link : String -> String -> Html Msg
link url label =
    if String.startsWith "mailto:" url then
        a [ href url, target "_blank" ] [ text label ]
    else
        a [ href url ] [ text label ]



-- Select is not currently used in the app, but I'm keeping it handy just in case...


select : (String -> msg) -> List ( String, String, Bool ) -> Html msg
select msg items =
    Html.select
        [ class "form-control"
        , onInput msg
        ]
        (List.map
            selectOption
            items
        )


selectOption : ( String, String, Bool ) -> Html msg
selectOption ( v, l, s ) =
    Html.option [ Attr.value v, Attr.selected s ] [ text l ]



-- Helper function: everyone's favorite idiom for use in style lists.
-- instead of ("bork","derf") you can write "bork" "derf"


-- Click handler that gives us the global mouse position.


-- offsetPosition : Json.Decode.Decoder Position
-- offsetPosition =
--     Json.Decode.map2 Position (Json.Decode.field "pageX" Json.Decode.int) (Json.Decode.field "pageY" Json.Decode.int)


-- mouseEvent : String -> (Position -> msg) -> VirtualDom.Property msg
-- mouseEvent event messager =
--     let
--         options =
--             { preventDefault = True, stopPropagation = True }
--     in
--         VirtualDom.onWithOptions event options (Json.Decode.map messager offsetPosition)


-- mouseClick : (Position -> msg) -> Attr msg
-- mouseClick =
--     mouseEvent "click"



-- SVG board.


boardView : Model -> Html Msg
boardView model =
    let
        lb =
            List.length model.board

        cellsize =
            boardDim // lb

        boardWidth = cellsize * lb

        -- boardHeight = (round (toFloat boardWidth / model.ratio))
        boardHeight = boardWidth

        attrs =
            [ viewBox ("0 0 " ++ (String.fromInt boardWidth) ++ " " ++ (String.fromInt boardHeight))
            , id "board"
            ]
    in
        div [class "board-wrapper"] [
            Svg.svg
                -- (if model.inputMode == UsingTouch then
                --     [ mouseClick MouseDown ] ++ attrs
                --  else
                --     attrs
                -- )
                attrs
                (boardSvg cellsize model.board) ]


boardSvg : Int -> Board -> List (Svg Msg)
boardSvg csize board =
    List.indexedMap (rowSvg csize) board
        |> List.concat


rowSvg : Int -> Int -> List Cell -> List (Svg Msg)
rowSvg csize row cells =
    List.indexedMap (cellSvg csize row) cells
        |> List.filterMap identity


cellSvg : Int -> Int -> Int -> Cell -> Maybe (Svg Msg)
cellSvg csize ypos xpos cell =
    if cell == Alive then
        Just
            (rect
                [ SAttr.width (String.fromInt (csize - 4))
                , SAttr.height (String.fromInt (csize - 4))
                , x (String.fromInt (xpos * csize))
                , y (String.fromInt (ypos * csize))
                ]
                []
            )
    else
        Nothing


-- Controls.


controlsView : Model -> Html Msg
controlsView model =
    div
        [ id "controls"]
        [ 
        editingControls model ]



editingControls : Model -> Html Msg
editingControls model =
    span [  ]
        [ a [ class "button is-small is-primary is-outlined", href "#", onClick ClearBoard ] [ text "Clear" ]
        , a [ class "button is-small is-primary is-outlined", href "#", onClick MakeSoup ] [ text "Soup" ]
        ]




-- Main information.
contentView : Html Msg
contentView =
    div [ id "content"] [
        div [ class "container is-fluid"] [
            div [class "tile is-ancestor"] [
                div [class "tile is-parent is-8"] [
                    div [class "tile is-child box blend"] [
                        p [class "title"] [
                            text "Stephan Lohwasser"]
                        ,p [class ""] [
                            a [class "fancy-link clickable", href "mailto:hello@lowi.dev"] [
                            text "hello@lowi.dev" ]]
                        ,p [] [
                            a [class "fancy-link clickable"
                             , href "https://www.google.com/maps/place/Springergasse+9,+1020+Wien/@48.2202182,16.3840917,16.15z"
                             , target "_blank"] [
                                text "Springergasse 9/29, 1020 Wien" ]
                            ]
                        ,p [] [
                            a [class "fancy-link clickable", href "tel:+4368110776554"] [
                            text "+43 681 10776554" ]]]]
                ]]]


-- Main view.


view : Model -> Html Msg
view model =
    div [ id "main" ]
            [ boardView model
            , contentView
            ]
    


-- Sample boards.


builtinBoards : List String
builtinBoards =
    [ """
!Name: Gosper glider gun
!Author: Bill Gosper
!The first known gun and the first known finite pattern with unbounded growth.
!Generates “gliders”, patterns that replicate themselves in slightly shifted
!positions.
!www.conwaylife.com/wiki/index.php?title=Gosper_glider_gun
........................O
......................O.O
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO
OO........O...O.OO....O.O
..........O.....O.......O
...........O...O
............OO
"""
    , """
!Name: Puffer 1
!Author: Bill Gosper
!An orthogonal, period-128 puffer and the first puffer to be discovered.
!www.conwaylife.com/wiki/index.php?title=Puffer_1
.OOO......O.....O......OOO
O..O.....OOO...OOO.....O..O
...O....OO.O...O.OO....O
...O...................O
...O..O.............O..O
...O..OO...........OO..O
..O...OO...........OO...O
"""
    , """
!Name: Various Oscillators
!These patterns repeat after various numbers of generations.
!www.conwaylife.com/wiki/Category:Oscillators
...............O
..............O.O
..............OO.O
..OO..........OO.OO...OO...........OO
..OO..........OO.O....OO...........OO
..............O.O
...............O






................OO.................OOO...OOO
...................................O..O.O..O
.O...OO....O..O...O................O.......O
OO...O.O...O..O....O
OO....OO...O....O.O.O...............O.....O
O................O.O.O...............OO.OO
..................O....O
...................O...O

....................OO


......O.....O
......O.....O
......OO...OO

..OOO..OO.OO..OOO
....O.O.O.O.O.O....................OO.....OO
......OO...OO......................OO.....OO

......OO...OO
....O.O.O.O.O.O
..OOO..OO.OO..OOO

......OO...OO
......O.....O
......O.....O
"""
    , """
!Name: Rectifier
!Author: Adam P. Goucher
!A 180-degree stable reflector with a recovery time of 106 generations.
!www.conwaylife.com/wiki/index.php?title=Rectifier
.O.O.......O
..OO......O.O
..O.......O.O
...........O











....................OO
....................OO

..OO
.O.O
.O
OO
...............................OO
..............................O..O..OO
..............................O.O....O
...........OO..................O.....O.OO
..........O.O.....................OO.O.O
..........O.......................O..O..O
.........OO....................O....O..OO
...............................OOOOO

.................................OO.O
.................................O.OO
"""
    , """
!Name: Herschel
!A heptomino shaped like the lowercase letter h, which occurs at generation 20 of the B-heptomino.
!An example of a “Methuselah” pattern that generates a large object before it finally stabilizes,
!typically after emitting one or more gliders.
!www.conwaylife.com/wiki/index.php?title=Herschel
O
OOO
O.O
..O
"""
    , """
!Name: Acorn
!Author: Charles Corderman
!A methuselah that stabilizes after 5206 generations.
!www.conwaylife.com/wiki/index.php?title=Acorn
.O
...O
OO..OOO
"""
    , """
!Name: Noah's ark
!Author: Charles Corderman
!A diagonal puffer made up of two switch engines that was found in 1971.
!A puffer is a pattern that moves itself but leaves debris behind.
!www.conwaylife.com/wiki/Noah%27s_ark
..........O.O
.........O
..........O..O
............OOO





.O
O.O

O..O
..OO
...O
"""
    ]
