'use strict';

// requires for webpack
// listing these files here is necessary, since webpack won't find them otherwise
require("./styles.scss");
require("../assets/fonts/IBMPlexMono-Regular.woff2");

const {Elm} = require('./Main');

const w = window.innerWidth
const h = window.innerHeight
const ratio = w / h
console.log('js - init', w, h, ratio)


var flags = { 
    hasTouch: isTouchDevice(), 
    randomSeed: Date.now(),
    ratio: ratio
 }

 var app = Elm.Main.init( { 
            node: document.getElementById("elm-node")
            , flags: flags } );


// On startup, the app will ask about the initial location of the
// SVG life board. This will happen after the initial display of
// the UI, so the information will be available.
app.ports.initialLocation.subscribe(boardLocation);

// In addition, anytime the window is resized, the displayed position
// and size of the board may change, so we need to tell our app
// about this. We can't use an "on" event handler inside Elm because
// we need to access the DOM to find out where the board is.
window.onresize = boardLocation;

// Look for the object with id "board" and return its location
// via an Elm port. I wanted to have a more general query function
// for later reuse, so a parameterless wrapper is needed.
function boardLocation() { objectLocation("#board"); }
function objectLocation(obj) {
    const e = document.querySelector(obj);
    const rect = e.getBoundingClientRect();
    app.ports.boardLocation.send({
        top: Math.round(rect.top),
        left: Math.round(rect.left),
        width: Math.round(rect.width),
        height: Math.round(rect.height)
        });

    const w = document.querySelector(".board-wrapper");
    const r = window.innerWidth / window.innerHeight
    if(r > 1) w.style.flexDirection = 'row'
    else w.style.flexDirection = 'column'
    }


// Detect if on touch device
function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}